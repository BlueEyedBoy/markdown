# GIT

Jedná se verzovací systém ideální jak pro práci v týmu tak pro práci samostatnou. K efektívnímů využívání Vám stačí jen pár příkazů, které si během chvilky osvojíte (zhruba 10). Systém umožňuje pracovat na jednom projektu více lidem současně a to bez koliz a jiných problémů. Konečné verze úprav jednotlivých členů se smergují do hlavní větvi a z ní dále pokračuje další vývoj

- **sudo apt-get install git**
	- nainstaluje git

- **git --help**

- **git init**
	- vytvoří ve složce základní osnovu pro git

- **git config --list**
	- vypíše veškerý config který je ve složce kde se nacházíte

- **git config --global user.name "username"**
	
- **git config user.name "username"**
	
- **git config --global user.email "email"**

- **git config user.email "email"**

- **git config --global --unset user.name**

- **git config --global --unset user.email**

- **git config --unset user.name**

- **git config --unset user.email**

- **git remote -v**
	- vypíše veškeré cesty dostuné ve složce

- **git remote add nazev cesta**
	- přidá cestu dostupnou přes název ve složce

- **git remote remove nazev**
	- odebere cestu dostupnou přes název ve složce

- **git clone nazev branche**

- **git pull nazev branche**

- **git fetch nazev branche**

- **git push nazev branche**

- **git push nazev branche --force**

- **git commit -m "text"**

- **git log**

- **git status**

- **git diff**

- **git diff commit**

- **git diff commit file**

- **git diff HEAD~3 file**
	- porovna aktuani stav s commitem o 3 zpatky

- **git add cesta**

- **git add -A**

- **git reset cesta**

- **git reset --soft**

- **git reset --hard**

- **git reset --soft commit**

- **git reset --hard commit**

- **git commit --amend**
	- prepise posledni commit se zmenami co jste udelal a mate pridany pro commit

- **git commit --amend -m "text"**
	- prepise posledni commit se zmenami co jste udelal a mate pridany pro commit a zmeni popis commitu

- **git branch**

- **git branch -D branche**

- **git checkout cesta**

	- vrati veskere zmeny v souboru od posledniho commitu

- **git checkout branche**

- **git checkout commit file**

	- vrati zmeny v soubru k tomu commitu

- **git checkout -b branche**

- **git merge branche**

### .gitignore

- **touch .gitignore**

- **gedit .gitignore**
	- '?' jeden znak

	- '*' nekolik znaku

	- '#' poznamka

	- budto nejdriv vytvorite .gitignore soubor a az pote soubory tere chce abz git ignoroval nebo v opacnem pripade ze jiz soubory mate vytvorene a pouze dodavate do .gitignore musite pouzit nasedujici prikaz

	- >*.log

	- >gin*

- **git rm --cached file**
	- smaze soubor ze synchroizace gitu a v pripade ze subor je dan v .gitignore tak ho nadale git uz bude ignorovat



```javascript
if (easy === true) {
   return true;
} else
   return false;
```

1. First list item
     - First nested list item
           - Second nested list item
     
Abyste to všechno pochopili stačí:

1. Instalace
   * bez toho to nejde 
1. Nastavení
     * zakladni nastaveni
     * pokrocile nastaveni
           * Jen tak dál
           * nikdy to nevzdavej
3. Použití

## Instalace

![obr 0.1](https://loremflickr.com/320/240)

> Může to snad být snažší?

## Nastavení

Použijeme:

```
git config
git remote

- user.name
- user.email
```

- origin path
- name test


### SSH

[Více o SSH]()

## Používání

### Stažení

### Odeslání

### Commitnutí

### Porovnání změn

## Pojmy


## Jazyk Markdown

Jedná se o jazyk v kterém je sán tento dokument a mohu vřele doporučit pro snadné, rychlé a přehledné psaní.

### [Markdown Viewer](https://chrome.google.com/webstore/detail/markdown-viewer/ckkdlimhmcjmikdlpkmbgfkaikojcbjk)
**je u něho potřeba nastavit přístup k URL souborm ve spravci doplnku**
pote jen staci otevrit v prohlizeci soubor

   **[Oficiální web](https://guides.github.com/features/mastering-markdown/)**
   
   *[Emoji](https://www.webpagefx.com/tools/emoji-cheat-sheet/)*
   
   ~~[Doporučuji](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)~~
   
   [Pokračování](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
